from django.apps import AppConfig


class CompressorAppConfig(AppConfig):
    name = 'compressor_app'
