from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import File
from asg2_1 import settings
from io import BytesIO
import zipfile
import os
import requests


# Create your views here.


def index(request):
    return JsonResponse(
        {
            "status": "OK",
            "message": "Server running"
        }
        , status=200
    )


@csrf_exempt
def compress(request):
    if request.method != "POST":
        return error_response("only POST method is allowed.")
    if "file" not in request.FILES:
        return error_response("no file attached, attach file with key name `file`.")
    if "access_token" not in request.POST:
        return error_response("no access token provided. provide with key name `access_token`")
    if not is_token_valid(request.POST["access_token"]):
        return error_response("access token is invalid or expired. Please change the token.")
    try:
        # Code legitimately stolen from
        # https://stackoverflow.com/questions/12881294/django-create-a-zip-of-multiple-files-and-make-it-downloadable
        # and edited appropriately

        id = save_file(request)

        file = File.objects.get(id=id)

        path = file.filepath
        filenames = [os.path.join(settings.MEDIA_ROOT, str(path))]

        zip_subdir = "result"
        zip_filename = "{}.zip".format(zip_subdir)

        s = BytesIO()

        zf = zipfile.ZipFile(s, "w")

        for fpath in filenames:
            fdir, fname = os.path.split(fpath)
            zip_path = os.path.join(zip_subdir, fname)

            zf.write(fpath, zip_path)

        zf.close()
        resp = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
        resp['Content-Disposition'] = 'attachment; filename={}'.format(zip_filename)

        return resp
    except Exception as e:
        return error_response("Server error while compressing your data. Please try again.")


def save_file(request):
    file = File(filepath=request.FILES["file"])
    file.save()
    return file.id


def is_token_valid(access_token):
    response = get_resource(access_token).json()
    if "error" in response:
        return False
    return True


def get_resource(access_token):
    url = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
    data_headers = {'Authorization': 'Bearer ' + str(access_token)}
    response = requests.get(url=url, headers=data_headers)
    return response


def error_response(message):
    return JsonResponse(
        {
            "status": "Error",
            "message": message
        }
        , status=400
    )
