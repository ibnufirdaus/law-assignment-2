from django.urls import path, re_path
from .views import *

app_name = "compressor_app"
urlpatterns = [
    path('', index, name='index'),
    path('compress/', compress, name='compress'),
]