from django.db import models
from datetime import datetime


class File(models.Model):
    filepath = models.FileField(upload_to='documents/')
